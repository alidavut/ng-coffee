class Temp
  @define: ->
  @use: ->
  @url: ->

class BaseClass
  @use: ->
    args = Array.prototype.slice.call(arguments, 0)
    if arguments.callee.caller.fn
      if arguments.callee.caller.fn.$inject
        arguments.callee.caller.fn.$inject = args.concat(arguments.callee.caller.fn.$inject)
      else
        arguments.callee.caller.fn.$inject = args
    else
      arguments.callee.caller.$inject = args

  @_inject: (caller, defaults=[]) ->
    if caller.$inject
      caller.fn.$inject = caller.$inject
    else
      caller.fn.$inject = defaults

class window.NgController extends BaseClass
  @define: (module, name) ->
    caller = arguments.callee.caller
    caller.fn = ->
      args = Array.prototype.slice.call(arguments, 0)
      instance = new (caller(Temp))()
      args.forEach (fn, i) -> instance[caller.fn.$inject[i]] = fn

      for name, val of instance.constructor.prototype
        continue if name in ['constructor', 'initialize']
        do (val) ->
          if typeof(val) == 'function'
            instance.$scope[name] = -> val.apply(instance, arguments)
          else
            instance.$scope[name] = val

      instance.initialize.apply(instance)

    @_inject(caller)
    caller.fn.$inject.push('$scope')

    angular.module(module).controller(name, caller.fn)


class window.NgComponent extends BaseClass
  @define: (module, name) ->
    caller = arguments.callee.caller
    caller.fn = ->
      args = Array.prototype.slice.call(arguments, 0)
      instance = new (caller(Temp))()
      args.forEach (fn, i) -> instance[caller.fn.$inject[i]] = fn
      {
        restrict: 'E'
        template: -> instance.template.apply(instance) if instance.template
        link: (scope, element, attrs) ->
          instance.scope = scope
          instance.element = element
          instance.attrs = attrs
          instance.initialize.apply(instance) if instance.initialize
      }

    @_inject(caller)
    angular.module(module).directive(name, caller.fn)

class window.NgDecorator extends BaseClass
  @define: (module, name) ->
    caller = arguments.callee.caller
    caller.fn = ->
      args = Array.prototype.slice.call(arguments, 0)
      instance = new (caller(Temp))()
      args.forEach (fn, i) -> instance[caller.fn.$inject[i]] = fn
      {
        restrict: 'A'
        template: -> instance.template.apply(instance) if instance.template
        link: (scope, element, attrs) ->
          instance.scope = scope
          instance.element = element
          instance.attrs = attrs
          instance.initialize.apply(instance) if instance.initialize
      }

    @_inject(caller)
    angular.module(module).directive(name, caller.fn)

class window.NgFormatter extends BaseClass
  @define: (module, name) ->
    caller = arguments.callee.caller
    caller.fn = ->
      instance = new (caller(Temp))()
      ->
        instance.initialize.apply(instance, arguments)
    angular.module(module).filter(name, caller.fn)

class window.NgObject extends BaseClass
  @define: (module, name) ->
    caller = arguments.callee.caller
    caller.fn = ->
      obj = caller(Temp)
      args = Array.prototype.slice.call(arguments, 0)
      args.forEach (fn, i) ->
        obj[caller.fn.$inject[i]] = fn
        obj.prototype[caller.fn.$inject[i]] = fn
      obj

    @_inject(caller)
    angular.module(module).service(name, caller.fn)

class window.NgModule extends BaseClass
  @define: (name) ->
    args = Array.prototype.slice.call(arguments, 1)
    module = angular.module(name, args)
    caller = arguments.callee.caller
    instance = new (caller(Temp))()
    caller.fn = ->
      args = Array.prototype.slice.call(arguments, 0)
      args.forEach (fn, i) -> instance[caller.fn.$inject[i]] = fn
      instance.initialize.apply(instance)

    @_inject(caller)
    module.run(caller.fn) if instance.initialize

class window.NgConfig extends BaseClass
  @define: (name) ->
    module = angular.module(name)
    caller = arguments.callee.caller
    instance = new (caller(Temp))()
    caller.fn = ->
      args = Array.prototype.slice.call(arguments, 0)
      args.forEach (fn, i) -> instance[caller.fn.$inject[i]] = fn
      instance.initialize.apply(instance)

    @_inject(caller)
    module.config(caller.fn) if instance.initialize