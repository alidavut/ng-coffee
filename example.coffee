class AnotherModule extends NgModule
  @define 'anotherModule'

class App extends NgModule
  @define 'app', 'anotherModule'
  @use '$rootScope'

  initialize: ->
    @$rootScope.appName = 'City List'
    console.log('The app just started')

class AppConfig extends NgConfig
  @define 'app'
  @use '$locationProvider'

  initialize: ->
    @$locationProvider.html5Mode(true)

class City extends NgObject
  @define 'app', 'City'
  @use '$http'

  @getCities: -> @$http.get('/cities.json')

class ListController extends NgController
  @define 'app', 'ListCtrl'
  @use '$timeout', 'City'

  initialize: ->
    @$scope.title = 'The City List'
    @$timeout =>
      @City.getCities().success (data) => @$scope.cities = data
    , 2000

  sayHello: (name) ->
    @$scope.title = "Hello #{name}!"

  cities: [
    {
      name: 'Default City'
      population: 20000000
    }
  ]

class Item extends NgComponent
  @define 'app', 'item'

  template: -> "
    <div>{{ city.name }} - {{ city.population }}</div>
  "

class Count extends NgDecorator
  @define 'app', 'count'
  @use '$interval'

  initialize: ->
    @count = parseInt(@attrs.startAt)
    @$interval((=> @element.html(++@count)), 1000)


class GtFilter extends NgFormatter
  @define 'app', 'gt'

  initialize: (@array, @gtValue) -> @compare()

  compare: ->
    @array.filter (item) => item.population > @gtValue

angular.bootstrap(document, ['app'])